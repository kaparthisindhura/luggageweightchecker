public class LuggageWeightChecker {
    double weight;
    LuggageWeightChecker(double weight)
    {
        this.weight=weight;
    }
    public void chargeCalculator(){
        double  weightLimit=15;
        try
        {
            if(weight-weightLimit>0.1)
            {
                throw new LimitExceededException();
            }
            else
            {
                System.out.println("Additional Cost : "+0);
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            System.out.println("Additional Cost : "+(weight-weightLimit)*500);

        }
    }
    public static void main(String[] args){

        LuggageWeightChecker bhavya=new LuggageWeightChecker(18);
        LuggageWeightChecker sruthi=new LuggageWeightChecker(15.1);
        System.out.println("Extra Charge Calculator for Bhavya");
        bhavya.chargeCalculator();
        System.out.println("Extra Charge Calculator for Shruthi");
        sruthi.chargeCalculator();
    }
}

